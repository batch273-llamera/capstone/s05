// alert('hello')

class Customer {
    constructor (email) {
        this.email = email;
        this.cart = new Cart ();
        this.orders = [];
    }

    checkOut() {
        if (this.cart.isEmpty()) {
            return 'Your cart is empty';
        } else {

        this.cart.computeTotal();

        const order = {
            products: this.cart.getContents(),
            totalAmount: this.cart.totalAmount
        };
        
        this.orders.push(order);

        this.cart.clearCartContents();
        console.log(order);
        return this;
        }
    }

}

// const john = new Customer('john@mail.com');
// console.log(john);

class Product {
    constructor (name, price, isActive = true) {
        this.name = name;
        this.price = price;
        this.isActive = isActive;
    }

    archive() {
        if(this.isActive) {
            this.isActive = false;
            return this;
        }
    }

    updatePrice(newPrice) {
        this.price = newPrice;
        return this;
    }

}

// const prodA = new Product('soap', 9.99);
// console.log(prodA);

class Cart {
    constructor (contents, totalAmount) {
        this.contents = [];
        this.totalAmount = 0;
    }

    addToCart(product, quantity) {
        this.contents.push({ product, quantity });     
        return this;
  	}

    showCartContents() {
        return this.contents;
    }

    updateProductQuantity(name, newQuantity) {
    const productIndex = this.contents.findIndex(item => item.product.name === name);
    if (productIndex === -1) {
      console.log('Product not found in cart');
      return;
    }

    this.contents[productIndex].quantity = newQuantity;
    return this;
  }

    clearCartContents() {
    	this.contents = [];
    	this.totalAmount = 0;
        return this;
  	}

    computeTotal() {
        let total = 0;
    	for (const item of this.contents) {
      	total += item.product.price * item.quantity;
    	}
    	this.totalAmount = total;
        return this;
  	}

  	isEmpty() {
    	return this.contents.length === 0;
  	}

  	getContents() {
    	return this.contents;
  	}

}

//const cart = new Cart();
// console.log(cart);
